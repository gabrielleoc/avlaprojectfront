import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';

const NewTask = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>{buttonLabel}Nueva Tarea</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Ingresa Tarea</ModalHeader>
        <ModalBody>
            <div>
                <div>
                    <div>
                        Titulo
                    </div>
                </div>
                <div>
                    <Input type='text' onInput={e => setTitle(e.target.value)} />
                </div>
                <div>
                    <div>
                        Description
                    </div>
                </div>
                <div>
                    <Input type='text' onInput={e => setDescription(e.target.value)} />
                </div>
            </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>Ingresar</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default NewTask;