import React from 'react';
import { Box, Typography } from '@material-ui/core';
import TaskComponent from './TaskComponent';
import NewTask from './NewTask'; 

const TasksContainer = (props) => {

    const { title, tasks, users , deleteCall, moveToDone, moveToDoing } = props;

    return (
        
        <Box>
            
            <Typography variant="h4"> {title} </Typography>
        {tasks.map(task=>(
            <div>
                <TaskComponent task={task} 
                deleteCall={deleteCall} 
                moveToDone={moveToDone} 
                moveToDoing={moveToDoing}></TaskComponent>
            </div>
        ))}
        </Box>   
    )


}


export default TasksContainer;