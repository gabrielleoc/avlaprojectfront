import React, { useState, useEffect } from 'react';
import './App.css';
import TasksContainer from './TasksContainer';
import 'bootstrap/dist/css/bootstrap.min.css';
import NewTask from './NewTask';
import {Grid} from '@material-ui/core';

//an example of class type component
const DrawTable = () => {

    const [todo, setTodo] = useState([]);
    const [doing, setDoing] = useState([]);
    const [done, setDone] = useState([]);
    const [users, setUsers] = useState([])

    const getTasks = async () => {
        const response = await fetch("http://localhost:8080/task");
        const tasks = await response.json()
        const todo = tasks.filter(t => t.columState === "TODO");
        const doing = tasks.filter(t => t.columState === "DOING");
        const done = tasks.filter(t => t.columState === "DONE");
        setTodo(todo);
        setDoing(doing);
        setDone(done);
    }

    const getUsers = async () => {
        const response = await fetch("http://localhost:8080/task/users");
        const users = await response.json();
        setUsers(users);
    }

    useEffect(() => {

        const fetchData = async () => {
            if(todo.length === 0 && doing.length === 0 && done.length === 0) {
                getTasks();
            }
    
            if(users.length === 0) {
                getUsers();
            }
        }

        fetchData();
      
        
    }, [])

    const deleteTask = async (taskId) => {

        fetch(`http://localhost:8080/task/${taskId}`, { method: "DELETE" })
            .then(response => {
                if(response.status === 200) {
                    getTasks();
                    getUsers()
                } else {
                    console.log(response)
                }
            })
            .catch(e => console.log(e))
    }

    const moveToDone = async (taskId) => {

        fetch(`http://localhost:8080/task/done/${taskId}`, { method: "PUT" })
            .then(response => {
                if(response.status === 200) {
                    getTasks();
                    getUsers()
                } else {
                    console.log(response)
                }
            })
            .catch(e => console.log(e))
    }

    const moveToDoing = async (taskId) => {

        fetch(`http://localhost:8080/task/doing/${taskId}`, { method: "PUT" })
            .then(response => {
                if(response.status === 200) {
                    getTasks();
                    getUsers();
                } else {
                    console.log(response)
                }
            })
            .catch(e => console.log(e))
    }


    
    return (
        <> 
        <NewTask></NewTask>
        <Grid   
            container
            direction="row"
            justify="space-evenly"
            alignItems="stretch"
        >  
            <TasksContainer 
            tasks={todo} 
            title={"TO DO"} 
            users={users} 
            deleteCall={deleteTask}
            moveToDone={moveToDone}
            moveToDoing={moveToDoing}
            />
            <TasksContainer 
            tasks={doing} 
            title={"DOING"} 
            users={users} 
            deleteCall={deleteTask}
            moveToDone={moveToDone}
            />
            <TasksContainer 
            tasks={done} 
            title={"DONE"} 
            users={users} 
            deleteCall={deleteTask}
            />
        </Grid>
        </>
        
            
        );
    
    }

export default DrawTable;
