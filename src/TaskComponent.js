import React from 'react';

import { Button } from '@material-ui/core';

const TaskComponent = (props) => {

    const { 
        task, 
        deleteCall, 
        moveToDone,
        moveToDoing
    } = props

    return (
        <>
            <div>
                <div style={{ fontWeight:"bold" }} >
                    Title
                </div>
                <div>
                    {task.name}
                </div>
                <div style={{ fontWeight:"bold" }}>
                    Description
                </div>
                <div>
                    {task.description}
                </div>
                <div style={{ fontWeight:"bold" }}>
                    Assigned User
                </div>
                <div>
                    {task.user ? task.user.user : ""}
                </div>
                <div>
                    <Button variant="contained">EDIT</Button>    
                    { moveToDoing && <Button variant="contained" color="primary" onClick={() => moveToDoing(task.id)}>DOING</Button>}
                    
                    { moveToDone && <Button variant="contained" color="primary"  onClick={() => moveToDone(task.id)}>DONE</Button> }
                    <Button variant="contained" color="secondary" onClick={() => deleteCall(task.id)}>DELETE</Button>
                </div>
            </div>
            
        </>
    )

}

export default TaskComponent;